module.exports = async ({
  getNamedAccounts,
  deployments,
  getChainId,
  getUnnamedAccounts,
}) => {
  const { deploy } = deployments;
  const { deployer } = await getNamedAccounts();

  // the following will only deploy "GenericMetaTxProcessor" if the contract was never deployed or if the code changed since last deployment
  await deploy("Greeter", {
    from: deployer,
    // gas: 4000000,
    args: ["Greeting set from ./deploy/Greeter.ts"],
  });
};

// To-Do create hh task
// const wallet = ethers.Wallet.fromMnemonic("test test test test test test test test test test test junk");
// console.log(wallet.privateKey)
// signerss.sendTransaction({to: "0x421b6D993A08d8e96c5B9A48aA006A39881E8173", value: ethers.utils.parseEther("1.0")});

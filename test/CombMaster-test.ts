require('dotenv').config();
const {
    expect
} = require("chai");
const {ethers, deployments, getNamedAccounts} = require("hardhat");
const {
    increase,
    duration
} = require("./time.js");


const {
    provider,
    constants,
    utils,
    BigNumber,
    getSigners,
    getContractFactory,
    Contract,
    Wallet
} = ethers;
// const masterchefABI = JSON.parse("[{\"inputs\":[{\"internalType\":\"contract CombToken\",\"name\":\"_comb\",\"type\":\"address\"}],\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"user\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"pid\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"Deposit\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"user\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"pid\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"EmergencyWithdraw\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"previousOwner\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"OwnershipTransferred\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"user\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"pid\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"Withdraw\",\"type\":\"event\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_allocPoint\",\"type\":\"uint256\"},{\"internalType\":\"contract IERC20\",\"name\":\"_lpToken\",\"type\":\"address\"},{\"internalType\":\"bool\",\"name\":\"_withUpdate\",\"type\":\"bool\"}],\"name\":\"add\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"comb\",\"outputs\":[{\"internalType\":\"contract CombToken\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_pid\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"_amount\",\"type\":\"uint256\"}],\"name\":\"deposit\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_pid\",\"type\":\"uint256\"}],\"name\":\"emergencyWithdraw\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_fromBlock\",\"type\":\"uint256\"}],\"name\":\"getPendingReward\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"lpTokenExistsInPool\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"massUpdatePools\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_pid\",\"type\":\"uint256\"}],\"name\":\"migrate\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"migrator\",\"outputs\":[{\"internalType\":\"contract IMigratorChef\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"owner\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_pid\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"_user\",\"type\":\"address\"}],\"name\":\"pendingComb\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"poolInfo\",\"outputs\":[{\"internalType\":\"contract IERC20\",\"name\":\"lpToken\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"allocPoint\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"lastRewardBlock\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"accCombPerShare\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"poolLength\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"renounceOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"rewardFirstBlock\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_blockNumber\",\"type\":\"uint256\"}],\"name\":\"rewardOnBlock\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"rewardThisBlock\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_pid\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"_allocPoint\",\"type\":\"uint256\"},{\"internalType\":\"bool\",\"name\":\"_withUpdate\",\"type\":\"bool\"}],\"name\":\"set\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"contract IMigratorChef\",\"name\":\"_migrator\",\"type\":\"address\"}],\"name\":\"setMigrator\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_startBlock\",\"type\":\"uint256\"}],\"name\":\"start\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"startBlock\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"totalAllocPoint\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"transferOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_lpTokenAddr\",\"type\":\"address\"},{\"internalType\":\"bool\",\"name\":\"_isExists\",\"type\":\"bool\"}],\"name\":\"updateLpTokenExists\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_pid\",\"type\":\"uint256\"}],\"name\":\"updatePool\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"userInfo\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"rewardDebt\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_pid\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"_amount\",\"type\":\"uint256\"}],\"name\":\"withdraw\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]");

const COMB_TOKEN = "0x7d36cce46dd2b0d28dde12a859c2ace4a21e3678";
const MASTERCHEF = "0x29430893589f627046776e906eFA281e410BEf69";
const UNISWAP_V2_ROUTER_02 = "0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D";
const UNISWAP_V2_FACTORY = "0x5C69bEe701ef814a2B6a3EDD4B1652CB9cc5aA6f";
const DEPOSIT_TOKEN = "0xaaD34b03d34Fe3dC17bE58fB9Bbb388739B234c2";
const MASTERCHEF_ABI = require("./../data/abi/contracts/MasterChef.sol/MasterChef.json");
const COMB_TOKEN_ABI = require("./../data/abi/contracts/CombToken.sol/CombToken.json");
const DEPOSIT_TOKEN_ABI = require("./../data/abi/contracts/DepositToken.sol/DepositToken.json");
const UNISWAP_V2_FACTORY_ABI = require("./../data/abi/@uniswap/v2-core/contracts/interfaces/IUniswapV2Factory.sol/IUniswapV2Factory.json");
const UNISWAP_V2_ROUTER_02_ABI = require("./../data/abi/@uniswap/v2-periphery/contracts/interfaces/IUniswapV2Router02.sol/IUniswapV2Router02.json");
const UNISWAP_V2_ERC20 = require("./../data/abi/@uniswap/v2-core/contracts/interfaces/IUniswapV2ERC20.sol/IUniswapV2ERC20.json");
const UNISWAP_V2_PAIR_ABI = require("./../data/abi/@uniswap/v2-core/contracts/interfaces/IUniswapV2Pair.sol/IUniswapV2Pair.json");

describe("CombMaster", function () {
    const { deploy } = deployments;

    let combToken,
        token,
        depositToken,
        uniswapV2Router02,
        uniswapV2Factory, masterchef, combMaster, eoa, signers, weth, provideLiquidity, uniswapV2ERC20, uniswapV2Pair;

    before(async () => {
        [...signers] = await getSigners();
        const ProvideLiquidity = await getContractFactory("ProvideLiquidity");
        const CombMaster = await getContractFactory("CombMaster");
        const Token = await getContractFactory("DepositToken");
        combToken = new Contract(COMB_TOKEN, COMB_TOKEN_ABI, signers[0]);
        depositToken = new Contract(DEPOSIT_TOKEN, DEPOSIT_TOKEN_ABI, signers[0]);
        uniswapV2Router02 = new Contract(UNISWAP_V2_ROUTER_02, UNISWAP_V2_ROUTER_02_ABI, signers[0]);
        uniswapV2Factory = new Contract(UNISWAP_V2_FACTORY, UNISWAP_V2_FACTORY_ABI, signers[0]);
        masterchef = new Contract(MASTERCHEF, MASTERCHEF_ABI, signers[0]);
        // provideLiquidity = await ProvideLiquidity.deploy();
        const { deployer } = await getNamedAccounts();
        console.log(deployer)
        await deploy("ProvideLiquidity", {
            from: deployer,
            // gas: 4000000,
            args: [],
        });
        console.log(provideLiquidity)

        combMaster = await CombMaster.deploy(combToken.address, depositToken.address, MASTERCHEF, provideLiquidity.address);
        token = await Token.connect(signers[0]).deploy();
        eoa = new Wallet(`0x${process.env.EOA_PRIVATE_KEY}`, provider);
        weth = await uniswapV2Router02.WETH();
        console.log("🚀 ~ file: CombMaster.js ~ line 46 ~ before ~ weth", weth)
    })

    xit("Should migrate", async function () {
        await depositToken.connect(eoa).transfer(combMaster.address, utils.parseEther("1"));
        await combMaster.depositOldMasterChef(10, utils.parseEther("1"));

        increase.hardhat(duration.days(1));

        const pendingComb = BigNumber.from(await masterchef.pendingComb(10, combMaster.address));
        await combMaster.depositOldMasterChef(10, 0);
        const balance = BigNumber.from(await combToken.balanceOf(combMaster.address));
        expect(balance).to.equal(pendingComb);
    });

    it("Should deposit combMaster: ", async () => {
        let wethTokenLP;

        await token.mint(signers[0].address, utils.parseEther("100"))
        await token.mint(signers[1].address, utils.parseEther("100"))
        await token.approve(UNISWAP_V2_ROUTER_02, utils.parseEther("100"));
        await uniswapV2Router02.addLiquidityETH(token.address, utils.parseEther("100"), 0, 0, signers[0].address, new Date().getTime() + 60, {
            value: utils.parseEther("100")
        });
        wethTokenLP = await uniswapV2Factory.getPair(token.address, weth);
        uniswapV2ERC20 = new Contract(wethTokenLP, UNISWAP_V2_ERC20, signers[0]);
        uniswapV2Pair = new Contract(wethTokenLP, UNISWAP_V2_PAIR_ABI, signers[0]);
        let reserves = await uniswapV2Pair.getReserves();
        console.log("🚀 ~ file: CombMaster.js ~ line 64 ~ it ~ reserves", reserves[0].toString())
        console.log("🚀 ~ file: CombMaster.js ~ line 65 ~ it ~ reserves", reserves[1].toString())
        console.log("🚀 ~ file: CombMaster.js ~ line 66 ~ it ~ reserves", reserves[2].toString())
       const lpBalance = await uniswapV2ERC20.balanceOf(signers[0].address);
        await uniswapV2ERC20.approve(combMaster.address, lpBalance._hex)

        await combMaster.start(new Date().getTime());
        await combMaster.add(100, wethTokenLP, true);
        let poolInfo = await combMaster.poolInfo(0);
        const poolLength = await combMaster.poolLength();

        await combMaster.connect(signers[1]).depositETH(0, 0, 0, 0, new Date().getTime() + 10, {
            value: utils.parseEther("2")
        });
        console.log('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa')
        poolInfo = await combMaster.poolInfo(0);
        console.log("🚀 ~ file: CombMaster.js ~ line 66 ~ it ~ poolInfo", poolInfo)
    })
});

import { HardhatUserConfig, task } from "hardhat/config";
import "@nomiclabs/hardhat-waffle";
import "@nomiclabs/hardhat-ethers";
import "hardhat-deploy-ethers";
import "hardhat-deploy";
import "@symfoni/hardhat-react";
import "hardhat-typechain";
import "@typechain/ethers-v5";
import "@nomiclabs/hardhat-ganache"
import "@nomiclabs/hardhat-web3"
import "@nomiclabs/hardhat-etherscan"
// import "@nomiclabs/hardhat-solpp"
import "@nomiclabs/hardhat-solhint"
import "hardhat-preprocessor"
import "@tenderly/hardhat-tenderly"
import "hardhat-spdx-license-identifier"
import "hardhat-abi-exporter"
import "hardhat-contract-sizer"
import "hardhat-log-remover"
import "hardhat-docgen"
import "hardhat-watcher"
import "hardhat-gas-reporter"
import "hardhat-tracer"
import * as dotenv from  "dotenv"

dotenv.config()
// env
const INFURA_KEY = process.env.INFURA_KEY || '';
const ALCHEMY_KEY = process.env.ALCHEMY_KEY || '';
const ETHERSCAN_KEY = process.env.ETHERSCAN_KEY || '';
const COINMARKETCUP_KEY = process.env.COINMARKETCUP_KEY || '';
const TENDERLY_PROJECT = process.env.TENDERLY_PROJECT || '';
const TENDERLY_USERNAME = process.env.TENDERLY_USERNAME || '';
const MNEMONIC = process.env.MNEMONIC || 'test test test test test test test test test test test junk';
const DEPLOYMENT_ACCOUNT_KEY = process.env.DEPLOYMENT_ACCOUNT_KEY || '';
const MAINNET_FORK = process.env.MAINNET_FORK || false;


// This is a sample Hardhat task. To learn how to create your own go to
// https://hardhat.org/guides/create-task.html
task("accounts", "Prints the list of accounts", async (args, hre) => {
  const accounts = await hre.ethers.getSigners();
  for (const account of accounts) {
    console.log(account.address);
  }
});

// You need to export an object to set up your config
// Go to https://hardhat.org/config/ to learn more

/**
 * @type import('hardhat/config').HardhatUserConfig
 */
const config = {
  react: {
    providerPriority: ["web3modal", "hardhat"],
  },
  networks: {
    hardhat: {
      inject: true, // optional. If true, it will EXPOSE your mnemonic in your frontend code. Then it would be available as an "in-page browser wallet" / signer which can sign without confirmation.
      chainId: 31337,
      // from: account[0]
      gas: "auto",
      gasPrice: "auto",
      gasMultiplier: 1,
      accounts: {
        mnemonic: MNEMONIC, //"test test test test test test test test test test test junk"
        initialIndex: 0,
        path: "m/44'/60'/0'/0",
        count: 60,
        accountsBalance: "10000000000000000000000"
      },
      blockGasLimit: 9500000,
      hardfork: "muirGlacier", // "byzantium", "constantinople", "petersburg", "istanbul" and "muirGlacier"
      throwOnTransactionFailures: true,
      throwOnCallFailures: true,
      loggingEnabled: false,
      // initialDate: date
      allowUnlimitedContractSize: false,
      forking: {
        url: "https://eth-mainnet.alchemyapi.io/v2/" + ALCHEMY_KEY,
        blockNumber: 12012745,
        enabled: MAINNET_FORK
      }
    },
    development: {
      url: "http://127.0.0.1:8545",
      chainId: 31337, //for ganache-cli 1337,
      // from: accounts[0]
      gas: "auto",
      gasPrice: "auto",
      gasMultiplier: 1,
      accounts: "remote",
      httpHeaders: undefined,
      timeout: 20000,
    },
    rinkeby: {
      url: "https://rinkeby.infura.io/v3/" + INFURA_KEY,
      chainId: 4,
      // from:
      gas: "auto",
      gasPrice: "auto",
      gasMultiplier: 1,
      accounts: [`0x${DEPLOYMENT_ACCOUNT_KEY}`],
      httpHeaders: undefined,
      timeout: 20000
    },
    mainnet: {
      url: "https://mainnet.infura.io/v3/" + INFURA_KEY,
      chainId: 1,
      // from:
      gas: "auto",
      gasPrice: "auto",
      gasMultiplier: 1,
      accounts: [`0x${DEPLOYMENT_ACCOUNT_KEY}`],
      httpHeaders: undefined,
      timeout: 20000
    },
  },
  etherscan: {
    // Your API key for Etherscan
    // Obtain one at https://etherscan.io/
    apiKey: ETHERSCAN_KEY
  },
  spdxLicenseIdentifier: {
    overwrite: true,
    runOnCompile: false,
  },
  abiExporter: {
    path: './data/abi',
    clear: false,
    flat: false,
    only: [],
    except: [],
  },
  contractSizer: {
    alphaSort: true,
    runOnCompile: false,
    disambiguatePaths: false,
  },
  docgen: {
    path: './docgen',
    clear: false,
    runOnCompile: true,
  },
  watcher: {
    compilation: {
      tasks: ["compile"],
      files: ["./contracts"],
      verbose: true,
    },
    ci: {
      tasks: ["clean", {
        command: "compile",
        params: {
          quiet: true
        }
      }, {
        command: "test",
        params: {
          noCompile: true,
          testFiles: ["testfile.ts"]
        }
      }],
    }
  },
  gasReporter: {
    enabled: false,
    currency: 'USD',
    coinmarketcap: COINMARKETCUP_KEY
  },
  tenderly: {
    project: TENDERLY_PROJECT,
    username: TENDERLY_USERNAME,
  },
  // solpp: {
  //   defs: {},
  //   cwd: "./contracts/",
  //   collapseEmptyLines: true,
  //   noPreprocessor: false,
  //   noFlatten: true,
  //   tolerant: false,
  // },
  solidity: {
    compilers: [{
      version: "0.8.0",
      settings: {
        optimizer: {
          enabled: true,
          runs: 200
        }
      }
    },
      {
        version: "0.7.6",
        settings: {
          optimizer: {
            enabled: true,
            runs: 200
          }
        }
      },
      {
        version: "0.7.0",
        settings: {
          optimizer: {
            enabled: true,
            runs: 200
          }
        }
      },
      {
        version: "0.6.12",
        settings: {
          optimizer: {
            enabled: true,
            runs: 200
          }
        }
      }
    ],
  },
  paths: {
    sources: "./contracts",
    tests: "./test",
    cache: "./cache",
    artifacts: "./artifacts"
  },
  mocha: {
    timeout: 20000
  }
};
export default config;

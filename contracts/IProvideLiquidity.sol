// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@uniswap/v2-core/contracts/interfaces/IUniswapV2Pair.sol";
import "./openzeppelin/contracts/token/ERC20/IERC20.sol";

interface IProvideLiquidity {
    function addLiquidityETH(
        IERC20,
        address,
        uint256,
        uint256,
        uint256,
        uint256
    ) external payable returns (uint256[3] memory);

    function addLiquidityETHByPair(
        IUniswapV2Pair,
        address,
        uint256,
        uint256,
        uint256,
        uint256
    ) external payable returns(uint256[] memory);

    // function removeLiquidityETH(
    //     address,
    //     uint256,
    //     uint8,
    //     uint256,
    //     uint256,
    //     uint256,
    //     address,
    //     uint256
    // ) external returns (uint256, uint256);

    function removeLiquidityETHByPair(
        IUniswapV2Pair,
        uint256,
        uint8,
        uint256,
        uint256,
        uint256,
        address payable,
        uint256
    ) external returns (uint256, uint256);

    function removeLiquidityETHWithPermit(
        address,
        uint256,
        uint256,
        uint256,
        uint256,
        uint256,
        address,
        uint8,
        uint8,
        bytes32,
        bytes32
    ) external returns (uint256[2] memory amounts);


}

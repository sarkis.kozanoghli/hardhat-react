// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@uniswap/v2-core/contracts/interfaces/IUniswapV2Pair.sol";
import "@uniswap/v2-periphery/contracts/interfaces/IUniswapV2Router02.sol";
import "@uniswap/v2-core/contracts/interfaces/IUniswapV2Factory.sol";
import "@uniswap/v2-core/contracts/interfaces/IUniswapV2ERC20.sol";
import "./openzeppelin/contracts/token/ERC20/IERC20.sol";
import "./openzeppelin/contracts/access/Ownable.sol";
import "hardhat/console.sol";
import "./IProvideLiquidity.sol";

contract ProvideLiquidity is IProvideLiquidity, Ownable {
    struct Uniswap {
        IUniswapV2Router02 uniswapV2Router02;
        IUniswapV2Factory uniswapV2Factory;
        IUniswapV2Pair uniswapV2Pair;
        IUniswapV2ERC20 wethToken;
    }

    Uniswap private _uniswap;

    constructor() {
        _uniswap.uniswapV2Router02 = IUniswapV2Router02(
            address(0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D)
        );
        _uniswap.uniswapV2Factory = IUniswapV2Factory(
            address(0x5C69bEe701ef814a2B6a3EDD4B1652CB9cc5aA6f)
        );
        _uniswap.wethToken = IUniswapV2ERC20(_uniswap.uniswapV2Router02.WETH());
    }

    fallback() external payable {}

    receive() external payable {}

    function addLiquidityETH(
        IERC20 _token,
        address _to,
        uint256 _getAmountsIn,
        uint256 _amountTokenMin,
        uint256 _amountETHMin,
        uint256 deadline
    ) external payable override returns (uint256[3] memory amounts) {
        require(
            msg.value > 1,
            "ERROR: Amount of sending ETH must be greater then 1"
        );

        address[] memory path = new address[](2);
        _token = IERC20(_token);

        path[0] = address(_uniswap.wethToken);
        path[1] = address(_token);
        uint256[] memory outputAmounts =
            _uniswap.uniswapV2Router02.swapExactETHForTokens{
                value: msg.value / 2
            }(_getAmountsIn, path, address(this), block.timestamp + deadline);

        _token.approve(address(_uniswap.uniswapV2Router02), outputAmounts[1]);

        (amounts[0], amounts[1], amounts[2]) = _uniswap
            .uniswapV2Router02
            .addLiquidityETH{value: msg.value / 2}(
            address(_token),
            outputAmounts[1],
            _amountTokenMin,
            _amountETHMin,
            _to,
            block.timestamp + deadline
        );
    }

    function addLiquidityETHByPair(
        IUniswapV2Pair _lptoken,
        address _to,
        uint256 _getAmountsIn,
        uint256 _amountTokenMin,
        uint256 _amountETHMin,
        uint256 deadline
    ) external payable override returns (uint256[] memory outputAmounts) {
        require(
            msg.value > 1,
            "ERROR: Amount of sending ETH must be greater then 1"
        );

        IUniswapV2Pair lptoken = IUniswapV2Pair(_lptoken);
        address[] memory path = new address[](2);
        (path[0], path[1]) = (lptoken.token0(), lptoken.token1());

        if (address(_uniswap.wethToken) != path[0]) {
            path[1] = path[0];
            path[0] = address(_uniswap.wethToken);
        }

        IERC20 token = IERC20(path[1]);
        outputAmounts = _uniswap.uniswapV2Router02.swapExactETHForTokens{
            value: msg.value / 2
        }(_getAmountsIn, path, address(this), block.timestamp + deadline);

        token.approve(address(_uniswap.uniswapV2Router02), outputAmounts[1]);

        (, , outputAmounts[0]) = _uniswap.uniswapV2Router02.addLiquidityETH{
            value: msg.value / 2
        }(
            address(token),
            outputAmounts[1],
            _amountTokenMin,
            _amountETHMin,
            _to,
            block.timestamp + deadline
        );
    }

    function withdrawAbandonedAssets(
        address _token,
        address _to,
        uint256 _amount
    ) external onlyOwner {
        if (_token == address(0)) {
            if (_amount == 0) {
                _amount = address(this).balance;
            }
            payable(_to).transfer(_amount);
        } else {
            if (_amount == 0) {
                _amount = IERC20(_token).balanceOf(address(this));
            }
            IERC20(_token).transfer(_to, _amount);
        }
    }

    function removeLiquidityETH(
        address _token,
        uint256 _liquidity,
        uint8 _choise,
        uint256 _amountTokenMin,
        uint256 _amountETHMin,
        uint256 _amountOutMin,
        address payable _to,
        uint256 _deadline
    ) public returns (uint256 amountToken, uint256 amountETH) {
        _uniswap.uniswapV2Pair = IUniswapV2Pair(
            address(
                _uniswap.uniswapV2Factory.getPair(
                    address(_uniswap.wethToken),
                    _token
                )
            )
        );

        _uniswap.uniswapV2Pair.transferFrom(
            msg.sender,
            address(this),
            _liquidity
        );
        _uniswap.uniswapV2Pair.approve(
            address(_uniswap.uniswapV2Router02),
            _liquidity
        );
        (amountToken, amountETH) = _uniswap
            .uniswapV2Router02
            .removeLiquidityETH(
            _token,
            _liquidity,
            _amountTokenMin,
            _amountETHMin,
            address(this),
            block.timestamp + _deadline
        );
        uint256[] memory outputAmounts;
        if (_choise == 1) {
            // onlY ETH
            outputAmounts = _swapExactTokensForETH(
                amountToken,
                _amountOutMin,
                _token,
                block.timestamp + _deadline
            );

            _to.transfer(outputAmounts[1] + amountETH);
            (amountToken, amountETH) = (0, outputAmounts[1] + amountETH);
            return (amountToken, amountETH);
        }
        if (_choise == 2) {
            // Only Token
            outputAmounts = _swapExactETHForTokens(
                amountETH,
                _amountOutMin,
                _token,
                block.timestamp + _deadline
            );
            IERC20(_token).transfer(_to, outputAmounts[1] + amountToken);
            (amountToken, amountETH) = (outputAmounts[1] + amountToken, 0);
            return (amountToken, amountETH);
        }
        //ETH and token
        _to.transfer(amountETH);
        IERC20(_token).transfer(_to, amountToken);
        return (amountToken, amountETH);
    }

    function removeLiquidityETHByPair(
        IUniswapV2Pair _lptoken,
        uint256 _liquidity,
        uint8 _choise,
        uint256 _amountTokenMin,
        uint256 _amountETHMin,
        uint256 _amountOutMin,
        address payable _to,
        uint256 _deadline
    ) public override returns (uint256 amountToken, uint256 amountETH) {
        IUniswapV2Pair lptoken = IUniswapV2Pair(_lptoken);

        address token = _getTokenFromPair(lptoken);

        lptoken.transferFrom(msg.sender, address(this), _liquidity);
        lptoken.approve(address(_uniswap.uniswapV2Router02), _liquidity);

        (amountToken, amountETH) = _uniswap
            .uniswapV2Router02
            .removeLiquidityETH(
            address(token),
            _liquidity,
            _amountTokenMin,
            _amountETHMin,
            address(this),
            _deadline
        );

        uint256[] memory outputAmounts;

        if (_choise == 1) {
            //Only ETH
            outputAmounts = _swapExactTokensForETH(
                amountToken,
                _amountOutMin,
                token,
                block.timestamp + _deadline
            );

            _to.transfer(outputAmounts[1] + amountETH);
            (amountToken, amountETH) = (0, outputAmounts[1] + amountETH);
            return (amountToken, amountETH);
        }

        if (_choise == 2) {
            // Only token
            outputAmounts = _swapExactETHForTokens(
                amountETH,
                _amountOutMin,
                token,
                block.timestamp + _deadline
            );
            IERC20(token).transfer(_to, amountToken + (outputAmounts[1]));
            (amountToken, amountETH) = (amountToken + outputAmounts[1], 0);
            return (amountToken, amountETH);
        }

        _to.transfer(amountETH);
        IERC20(token).transfer(_to, amountToken);
        return (amountToken, amountETH);
    }

    function removeLiquidityETHWithPermit(
        address _token,
        uint256 _liquidity,
        uint256 _amountTokenMin,
        uint256 _amountETHMin,
        uint256 _amountOutMin,
        uint256 _deadline,
        address _to,
        uint8 _choise,
        uint8 _v,
        bytes32 _r,
        bytes32 _s
    ) external override returns (uint256[2] memory amounts) {
        IUniswapV2ERC20(
            _uniswap.uniswapV2Factory.getPair(
                address(_uniswap.wethToken),
                _token
            )
        )
            .permit(
            msg.sender,
            address(this),
            _liquidity,
            _deadline,
            _v,
            _r,
            _s
        );
        (amounts[0], amounts[1]) = removeLiquidityETH(
            _token,
            _liquidity,
            _choise,
            _amountTokenMin,
            _amountETHMin,
            _amountOutMin,
            payable(_to),
            _deadline
        );
    }

    function _getTokenFromPair(IUniswapV2Pair _lptoken)
        internal
        view
        returns (address token)
    {
        token = _lptoken.token0();
        if (token == address(_uniswap.wethToken)) {
            token = _lptoken.token1();
        }
    }

    function _swapExactETHForTokens(
        uint256 _amountIn,
        uint256 _amountOutMin,
        address _token,
        uint256 _deadline
    ) internal returns (uint256[] memory amounts) {
        address[] memory path = new address[](2);
        (path[0], path[1]) = (address(_uniswap.wethToken), _token);
        amounts = _uniswap.uniswapV2Router02.swapExactETHForTokens{
            value: _amountIn
        }(_amountOutMin, path, address(this), block.timestamp + _deadline);
    }

    function _swapExactTokensForETH(
        uint256 _amountIn,
        uint256 _amountOutMin,
        address _token,
        uint256 _deadline
    ) internal returns (uint256[] memory amounts) {
        address[] memory path = new address[](2);
        (path[0], path[1]) = (_token, address(_uniswap.wethToken));

        IERC20(_token).approve(address(_uniswap.uniswapV2Router02), _amountIn);

        amounts = _uniswap.uniswapV2Router02.swapExactTokensForETH(
            _amountIn,
            _amountOutMin,
            path,
            address(this),
            block.timestamp + _deadline
        );
    }
}

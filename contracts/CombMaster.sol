// SPDX-License-Identifier: MIT

pragma solidity 0.8.0;

import "@uniswap/v2-core/contracts/interfaces/IUniswapV2Pair.sol";
import "@uniswap/v2-periphery/contracts/interfaces/IUniswapV2Router02.sol";
import "@uniswap/v2-core/contracts/interfaces/IUniswapV2Factory.sol";
import "@uniswap/v2-core/contracts/interfaces/IUniswapV2ERC20.sol";
import "./openzeppelin/contracts/token/ERC20/IERC20.sol";
import "./openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "./openzeppelin/contracts/utils/structs/EnumerableSet.sol";
import "./openzeppelin/contracts/access/Ownable.sol";
import "./ICombMaster.sol";
import "./IProvideLiquidity.sol";
import "hardhat/console.sol";

// MasterChef is the master of Comb. He can make Comb and he is a fair guy.
//
// Note that it's ownable and the owner wields tremendous power. The ownership
// will be transferred to a governance smart contract once COMB is sufficiently
// distributed and the community can show to govern itself.
//
// Have fun reading it. Hopefully it's bug-free. God bless.
contract CombMaster is Ownable {
    using SafeERC20 for IERC20;

    // Info of each user.
    struct UserInfo {
        uint256 amount; // How many LP tokens the user has provided.
        uint256 rewardDebt; // Reward debt. See explanation below.
        //
        // We do some fancy math here. Basically, any point in time, the amount of COMBs
        // entitled to a user but is pending to be distributed is:
        //
        //   pending reward = (user.amount * pool.accCombPerShare) - user.rewardDebt
        //
        // Whenever a user deposits or withdraws LP tokens to a pool. Here's what happens:
        //   1. The pool's `accCombPerShare` (and `lastRewardBlockTimestamp`) gets updated.
        //   2. User receives the pending reward sent to his/her address.
        //   3. User's `amount` gets updated.
        //   4. User's `rewardDebt` gets updated.
    }

    // Info of each pool.
    struct PoolInfo {
        IERC20 lpToken; // Address of LP token contract.
        uint256 allocPoint; // How many allocation points assigned to this pool. COMBs to distribute per block.
        uint256 lastRewardBlockTimestamp; // Last block number that COMBs distribution occurs.
        uint256 accCombPerShare; // Accumulated COMBs per share, times 1e12. See below.
    }

    struct Uniswap {
        address uniswapV2Router02Addr;
        address uniswapV2FactoryAddr;
        // address wethTokenAddr;
        // address lpTokenAddr;
        IUniswapV2Router02 uniswapV2Router02;
        IUniswapV2Factory uniswapV2Factory;
        IUniswapV2ERC20 wethToken;
        IUniswapV2ERC20 lpToken;
    }

    // The COMB TOKEN!
    IERC20 public comb;
    // token for deposit in old contract
    IERC20 public depositToken;
    // old CombMaster
    ICombMaster public masterChef;
    IProvideLiquidity private liquidityProvider;
    // Maximum COMB tokens to be mined
    uint256 MAX_COMB_SUPPLY = 10000 ether;
    // COMB tokens created per day.
    uint256 public combPerSeconds = 23148148 * 10**7;
    // COMB tokens created on first block.

    // Info of each pool.
    PoolInfo[] public poolInfo;
    mapping(address => bool) public lpTokenExistsInPool;
    // Info of each user that stakes LP tokens.
    mapping(uint256 => mapping(address => UserInfo)) public userInfo;
    // Total allocation poitns. Must be the sum of all allocation points in all pools.
    uint256 public totalAllocPoint = 0;
    // The block number when COMB mining starts.
    uint256 public startTimestamp = 0;

    Uniswap private _uniswap;
    IUniswapV2ERC20 private _combToken;

    event Deposit(address indexed user, uint256 indexed pid, uint256 amount);
    event Withdraw(address indexed user, uint256 indexed pid, uint256 amount);
    event EmergencyWithdraw(
        address indexed user,
        uint256 indexed pid,
        uint256 amount
    );

    fallback() external payable {}

    receive() external payable {}

    constructor(
        IERC20 _comb,
        IERC20 _depositToken,
        ICombMaster _masterChef,
        IProvideLiquidity _liquidityProvider
    ) {
        comb = _comb;
        depositToken = _depositToken;
        masterChef = _masterChef;
        liquidityProvider = _liquidityProvider;

        _combToken = IUniswapV2ERC20(address(_comb));
        _uniswap.uniswapV2Router02Addr = address(
            0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D
        );
        _uniswap.uniswapV2FactoryAddr = address(
            0x5C69bEe701ef814a2B6a3EDD4B1652CB9cc5aA6f
        );
        _uniswap.uniswapV2Router02 = IUniswapV2Router02(
            _uniswap.uniswapV2Router02Addr
        );
        _uniswap.uniswapV2Factory = IUniswapV2Factory(
            _uniswap.uniswapV2FactoryAddr
        );
        // _uniswap.wethTokenAddr = _uniswap.uniswapV2Router02.WETH();
        // _uniswap.lpTokenAddr = _uniswap.uniswapV2Factory.getPair(
        //     _uniswap.wethTokenAddr,
        //     address(_comb)
        // );
        _uniswap.wethToken = IUniswapV2ERC20(_uniswap.uniswapV2Router02.WETH());
        // _uniswap.lpToken = IUniswapV2ERC20(_uniswap.lpTokenAddr);
    }

    function poolLength() external view returns (uint256) {
        return poolInfo.length;
    }

    function start(uint256 _startTimestamp) external onlyOwner {
        startTimestamp = _startTimestamp;
    }

    // Add a new lp to the pool. Can only be called by the owner.
    function add(
        uint256 _allocPoint,
        IERC20 _lpToken,
        bool _withUpdate
    ) public onlyOwner {
        require(startTimestamp != 0, "Please start the contract first!");
        require(
            !lpTokenExistsInPool[address(_lpToken)],
            "MasterCheif: LP Token Address already exists in pool"
        );
        if (_withUpdate) {
            massUpdatePools();
        }
        uint256 lastRewardBlockTimestamp =
            block.timestamp > startTimestamp ? block.timestamp : startTimestamp;
        totalAllocPoint = totalAllocPoint + _allocPoint;
        poolInfo.push(
            PoolInfo({
                lpToken: _lpToken,
                allocPoint: _allocPoint,
                lastRewardBlockTimestamp: lastRewardBlockTimestamp,
                accCombPerShare: 0
            })
        );
        lpTokenExistsInPool[address(_lpToken)] = true;
    }

    function updateLpTokenExists(address _lpTokenAddr, bool _isExists)
        external
        onlyOwner
    {
        lpTokenExistsInPool[_lpTokenAddr] = _isExists;
    }

    // Update the given pool's COMB allocation point. Can only be called by the owner.
    function set(
        uint256 _pid,
        uint256 _allocPoint,
        bool _withUpdate
    ) public onlyOwner {
        if (_withUpdate) {
            massUpdatePools();
        }
        totalAllocPoint =
            totalAllocPoint -
            poolInfo[_pid].allocPoint +
            _allocPoint;
        poolInfo[_pid].allocPoint = _allocPoint;
    }

    function rewardOnBlock(uint256 _blockTimestamp)
        public
        view
        returns (uint256 _reward)
    {
        _reward = (_blockTimestamp - startTimestamp) * combPerSeconds;
    }

    // Return reward unclaimed reward since last block
    function getPendingReward(uint256 _fromBlockTimestamp)
        public
        view
        returns (uint256)
    {
        uint256 secondsPassedAfter = block.timestamp - _fromBlockTimestamp;
        uint256 reward =
            rewardOnBlock(_fromBlockTimestamp) +
                (rewardOnBlock(block.timestamp) / 2) *
                secondsPassedAfter;

        uint256 totalCombSupply = comb.totalSupply();
        if (totalCombSupply + reward > MAX_COMB_SUPPLY) {
            reward = MAX_COMB_SUPPLY - totalCombSupply;
        }
        return reward;
    }

    // View function to see pending COMBs on frontend.
    function pendingComb(uint256 _pid, address _user)
        external
        view
        returns (uint256)
    {
        PoolInfo storage pool = poolInfo[_pid];
        UserInfo storage user = userInfo[_pid][_user];
        uint256 accCombPerShare = pool.accCombPerShare;
        uint256 lpSupply = pool.lpToken.balanceOf(address(this));
        if (block.timestamp > pool.lastRewardBlockTimestamp && lpSupply != 0) {
            uint256 pendingReward =
                getPendingReward(pool.lastRewardBlockTimestamp);
            uint256 combReward =
                (pendingReward * pool.allocPoint) / totalAllocPoint;
            accCombPerShare = accCombPerShare + (combReward * 1e12) / lpSupply;
        }
        return (user.amount * accCombPerShare) / 1e12 - user.rewardDebt;
    }

    // Update reward variables for all pools. Be careful of gas spending!
    function massUpdatePools() public {
        uint256 length = poolInfo.length;
        for (uint256 pid = 0; pid < length; ++pid) {
            updatePool(pid);
        }
    }

    // Update reward variables of the given pool to be up-to-date.
    function updatePool(uint256 _pid) public {
        PoolInfo storage pool = poolInfo[_pid];
        if (block.timestamp <= pool.lastRewardBlockTimestamp) {
            return;
        }
        uint256 lpSupply = pool.lpToken.balanceOf(address(this));
        if (lpSupply == 0) {
            pool.lastRewardBlockTimestamp = block.timestamp;
            return;
        }
        uint256 pendingReward = getPendingReward(pool.lastRewardBlockTimestamp);
        uint256 combReward =
            (pendingReward * pool.allocPoint) / totalAllocPoint;
        pool.accCombPerShare =
            pool.accCombPerShare +
            (combReward * 1e12) /
            lpSupply;
        pool.lastRewardBlockTimestamp = block.timestamp;
    }

    // Deposit LP tokens to MasterChef for COMB allocation.
    function deposit(
        uint256 _pid,
        uint256 _amount,
        bool caller
    ) public {
        PoolInfo storage pool = poolInfo[_pid];
        UserInfo storage user = userInfo[_pid][msg.sender];
        updatePool(_pid);
        if (user.amount > 0) {
            uint256 pending =
                (user.amount * pool.accCombPerShare) / 1e12 - user.rewardDebt;
            if (pending > 0) {
                safeCombTransfer(msg.sender, pending);
            }
        }
        if (_amount > 0) {
            if (!caller)
                pool.lpToken.safeTransferFrom(
                    address(msg.sender),
                    address(this),
                    _amount
                );
            user.amount = user.amount + _amount;
        }
        user.rewardDebt = (user.amount * pool.accCombPerShare) / 1e12;
        emit Deposit(msg.sender, _pid, _amount);
    }

    function depositETH(
        uint256 _getAmountsIn,
        uint256 _amountTokenMin,
        uint256 _amountETHMin,
        uint256 _poolId,
        uint256 _deadline
    ) public payable {
        require(msg.value > 1, "Amount can not be 0");
        IUniswapV2Pair lptoken = IUniswapV2Pair(address(poolInfo[_poolId].lpToken));

        liquidityProvider.addLiquidityETHByPair{value: msg.value}(
            lptoken,
            address(this),
            _getAmountsIn,
            _amountTokenMin,
            _amountETHMin,
            _deadline
        );
    }

    // Deposit LP tokens to old CombMaster contract.
    function depositOldMasterChef(uint256 _pid, uint256 _amount)
        public
        onlyOwner
    {
        if (_amount > 0) depositToken.approve(address(masterChef), _amount);
        masterChef.deposit(_pid, _amount);
    }

    // Withdraw LP tokens from MasterChef.
    function withdraw(uint256 _pid, uint256 _amount) public {
        PoolInfo storage pool = poolInfo[_pid];
        UserInfo storage user = userInfo[_pid][msg.sender];
        require(user.amount >= _amount, "withdraw: not good");
        updatePool(_pid);
        uint256 pending =
            (user.amount * pool.accCombPerShare) / 1e12 - user.rewardDebt;
        if (pending > 0) {
            safeCombTransfer(msg.sender, pending);
        }
        if (_amount > 0) {
            user.amount = user.amount - _amount;
            pool.lpToken.safeTransfer(address(msg.sender), _amount);
        }
        user.rewardDebt = (user.amount * pool.accCombPerShare) / 1e12;
        emit Withdraw(msg.sender, _pid, _amount);
    }

    // Withdraw without caring about rewards. EMERGENCY ONLY.
    function emergencyWithdraw(uint256 _pid) public {
        PoolInfo storage pool = poolInfo[_pid];
        UserInfo storage user = userInfo[_pid][msg.sender];
        pool.lpToken.safeTransfer(address(msg.sender), user.amount);
        emit EmergencyWithdraw(msg.sender, _pid, user.amount);
        user.amount = 0;
        user.rewardDebt = 0;
    }

    // Safe comb transfer function, just in case if rounding error causes pool to not have enough COMBs.
    function safeCombTransfer(address _to, uint256 _amount) internal {
        uint256 combBal = comb.balanceOf(address(this));
        if (_amount > combBal) {
            comb.transfer(_to, combBal);
        } else {
            comb.transfer(_to, _amount);
        }
    }
}

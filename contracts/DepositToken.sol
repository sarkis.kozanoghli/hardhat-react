// SPDX-License-Identifier: MIT

pragma solidity ^0.6.12;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/math/SafeMath.sol";

// CombToken with Governance.
contract DepositToken is ERC20("Deposit.Token", "DPT"), Ownable {

    uint256 MAX_SUPPLY = 10000 ether;

    /// @notice Creates `_amount` token to `_to`. Must only be called by the owner (MasterChef).
    function mint(address _to, uint256 _amount) public onlyOwner {
        // Make sure max supply never passed
        require(totalSupply().add(_amount) <= MAX_SUPPLY, 'Max supply reached');

        _mint(_to, _amount);
    }
}